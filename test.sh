#!/bin/bash

url="https://ipinfo.io"
string="^(([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.){3}([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))$"

usage() {
        echo "Usage: $0 [-c -i -h ] [ip addr]"
        echo " -c csv format"
        echo " -i only for gitlab ci test stage"
        echo " -h help"
}

# stdin
if [ -t 0 ]; then
  stdin=$1
else
  stdin=$(cat)
fi

# options
while getopts "c:i:h" opt; do
  case ${opt} in
    c )
      if [[ "$2" =~ $string ]]; then
        output1=$(curl -s $url/$2)
#       ip1=$(echo $output1 | jq -r .ip)
        city1=$(echo $output1 | jq -r .city)
        org1=$(echo $output1 | jq -r .org | awk -F ' ' '{$1="";print $0}')
        asn1=$(echo $output1 | jq -r .org | awk -F ' ' '{print $1}')
        echo -ne "$city1\n$org1\n$asn1" | paste -sd ";"
      else
        echo "Invalid IP address"
      fi
    exit 0 ;;
    i )
      if [[ "$2" =~ $string ]]; then
        output=$(curl -s $url/$2)
        city=$(echo $output | jq -r .city)
        org=$(echo $output | jq -r .org | awk -F' ' '{$1=""; print $0}')
        asn=$(echo $output | jq -r .org | awk -F ' ' '{print $1}')
        echo -ne "city: $city\norg:$org\nasn: $asn\n"
      else
        echo "Invalid IP address"
      fi
    exit 0 ;;
    h )
      usage
    exit 0 ;;
#    \? )
#    echo "Invalid option: $OPTARG"
#    exit 0 ;;
    * )
    usage
    exit 0 ;;
    esac
done
shift $((OPTIND -1))

# output
if [[ "$stdin" =~ $string ]]; then
  output=$(curl -s $url/$stdin)
  city=$(echo $output | jq -r .city)
  org=$(echo $output | jq -r .org | awk -F' ' '{$1=""; print $0}')
  asn=$(echo $output | jq -r .org | awk -F ' ' '{print $1}')
  echo -ne "city: $city\norg:$org\nasn: $asn\n"
else
  echo "Invalid IP address"
fi

