FROM centos:7
MAINTAINER Pawel Winczewski

# install
RUN yum -y install epel-release
RUN yum --nogpgcheck -y update && \
yum -y install git curl jq docker \
yum clean all

# path
ENV NOTVISIBLE "user profile"
RUN echo 'export PATH="/test:$PATH"' >> /root/.bashrc

# test script
RUN git clone https://www.gitlab.com/zaaraz/test.git

# porty
# EXPOSE 22



